use odeint::system::System;
use std::ops::{AddAssign, Mul};

pub struct Iter<A, B>
    where A: System,
          A: System<State = B>,
          B: AddAssign,
          B: Mul<f64>,
          B: Mul<f64, Output = B>,
          B: Clone
{
    system: A,
    state: B,
    t: f64,
    dt: f64,
}

impl<A, B> Iter<A, B>
    where A: System,
          A: System<State = B>,
          B: AddAssign,
          B: Mul<f64>,
          B: Mul<f64, Output = B>,
          B: Clone
{
    pub fn new(system: A, init: B, step: f64) -> Iter<A, B> {
        Iter {
            system: system,
            state: init,
            t: 0.0,
            dt: step,
        }
    }
}

impl<A, B> Iterator for Iter<A, B>
    where A: System,
          A: System<State = B>,
          B: AddAssign,
          B: Mul<f64>,
          B: Mul<f64, Output = B>,
          B: Clone
{
    type Item = (f64, B);
    fn next(&mut self) -> Option<(f64, B)> {
        self.state += self.system.diff(&self.state, self.t) * self.dt;
        self.t = self.t + self.dt;
        Some((self.t, self.state.clone()))
    }
}
