pub trait System {
    type State;
    fn diff(&self, &Self::State, t: f64) -> Self::State;
}
