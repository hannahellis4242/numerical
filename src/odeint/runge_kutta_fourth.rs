use odeint::system::System;
use std::ops::{Add, AddAssign, Mul};

pub struct Iter<A, B>
    where A: System,
          A: System<State = B>,
          B: AddAssign,
          B: Add,
          B: Add<Output = B>,
          B: Mul<f64>,
          B: Mul<f64, Output = B>,
          B: Clone
{
    system: A,
    state: B,
    t: f64,
    halfstep: f64,
}

impl<A, B> Iter<A, B>
    where A: System,
          A: System<State = B>,
          B: AddAssign,
          B: Add,
          B: Add<Output = B>,
          B: Mul<f64>,
          B: Mul<f64, Output = B>,
          B: Clone
{
    pub fn new(system: A, init: B, step: f64) -> Iter<A, B> {
        Iter {
            system: system,
            state: init.clone(),
            t: 0.0,
            halfstep: step / 2.0,
        }
    }
}

impl<A, B> Iterator for Iter<A, B>
    where A: System,
          A: System<State = B>,
          B: AddAssign,
          B: Add,
          B: Add<Output = B>,
          B: Mul<f64>,
          B: Mul<f64, Output = B>,
          B: Clone
{
    type Item = (f64, B);
    fn next(&mut self) -> Option<(f64, B)> {
        let k1 = self.system.diff(&self.state, self.t);
        let k2 = self.system.diff(&(self.state.clone() + k1.clone() * self.halfstep),
                                  self.t + self.halfstep);
        let k3 = self.system.diff(&(self.state.clone() + k2.clone() * self.halfstep),
                                  self.t + self.halfstep);
        let k4 = self.system.diff(&(self.state.clone() + k3.clone() * 2.0 * self.halfstep),
                                  self.t + 2.0 * self.halfstep);
        self.state += (k1 + k2 * 2.0 + k3 * 2.0 + k4) * (self.halfstep / 3.0);
        self.t = self.t + 2.0 * self.halfstep;
        Some((self.t, self.state.clone()))
    }
}
