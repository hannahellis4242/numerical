pub struct Iter<T>
    where T: Fn(f64) -> f64
{
    f: T,
    range: (f64, f64),
    values: (f64, f64),
}

impl<T> Iter<T>
    where T: Fn(f64) -> f64
{
    pub fn new(func: T, range: (f64, f64)) -> Iter<T> {
        let v = (func(range.0), func(range.1));
        Iter {
            f: func,
            range: range,
            values: v,
        }
    }
}

impl<T> Iterator for Iter<T>
    where T: Fn(f64) -> f64
{
    type Item = (f64, f64);
    fn next(&mut self) -> Option<(f64, f64)> {
        let mid = (self.range.0 + self.range.1) / 2.0;
        let mid_value = {
            let f = &self.f;
            f(mid)
        };
        let positive_gradient = self.values.0 < self.values.1;
        if (positive_gradient && mid_value < 0.0) || (!positive_gradient && 0.0 < mid_value) {
            self.range = (mid, self.range.1);
            self.values = (mid_value, self.values.1);
        } else {
            self.range = (self.range.0, mid);
            self.values = (self.values.0, mid_value);
        }
        Some(self.range)
    }
}
