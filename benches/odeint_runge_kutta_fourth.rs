#[macro_use]
extern crate bencher;
extern crate rulinalg;

use rulinalg::vector::Vector;
use bencher::Bencher;

extern crate numerical;

fn empty(b: &mut Bencher) {
    b.iter(|| 1)
}

struct Decay {
    decay_rate: f64,
}
impl numerical::odeint::system::System for Decay {
    type State = rulinalg::vector::Vector<f64>;
    fn diff(&self, state: &rulinalg::vector::Vector<f64>, t: f64) -> rulinalg::vector::Vector<f64> {
        rulinalg::vector::Vector::new(vec![-self.decay_rate * state[0], self.decay_rate * state[0]])
    }
}

fn decay_example(b: &mut Bencher) {
    b.iter(|| {
        let iter =
        numerical::odeint::runge_kutta_fourth::Iter::new(Decay { decay_rate: 0.2 },
                                                         rulinalg::vector::Vector::new(vec![100.0,
                                                                                           0.0]),
                                                         0.02);
        //println!("0\t100.0\t0.0\t100.0");
        // for (t, v) in iter.take(200) {
        //     println!("{:?}\t{:?}\t{:?}\t{:?}", t, v[0], v[1], v[0] + v[1]);
        // }
        let result = iter.take(20).collect::<Vec<(f64, rulinalg::vector::Vector<f64>)>>();
    })
}

struct Lorentz {
    sigma: f64,
    R: f64,
    b: f64,
}

impl numerical::odeint::system::System for Lorentz {
    type State = rulinalg::vector::Vector<f64>;
    fn diff(&self, x: &rulinalg::vector::Vector<f64>, _: f64) -> rulinalg::vector::Vector<f64> {
        let mut dxdt = rulinalg::vector::Vector::zeros(3);
        dxdt[0] = self.sigma * (x[1] - x[0]);
        dxdt[1] = self.R * x[0] - x[1] - x[0] * x[2];
        dxdt[2] = -self.b * x[2] + x[0] * x[1];
        dxdt
    }
}

fn lorentz_example(b: &mut Bencher) {
    b.iter(|| {
        let iter =
        numerical::odeint::runge_kutta_fourth::Iter::new(Lorentz { sigma:10.0,R:28.0,b:8.0/3.0 },
                                                         rulinalg::vector::Vector::new(vec![10.0,
                                                                                           0.0,0.0]),
                                                         0.1);
        /*println!("0\t10.0\t0.0\t0");
        for (t, v) in iter.take(200) {
             println!("{:?}\t{:?}\t{:?}\t{:?}", t, v[0], v[1], v[2] );
        }*/
        let result = iter.take(20).collect::<Vec<(f64, rulinalg::vector::Vector<f64>)>>();
    })
}

benchmark_group!(benches, empty, decay_example, lorentz_example);
benchmark_main!(benches);

#[cfg(never)]
fn main() {}
