#[cfg(test)]
#[macro_use]
extern crate hamcrest;
use hamcrest::prelude::*;

extern crate numerical;

use std::ops::{AddAssign, Mul};

struct SimpleExample {}

impl numerical::odeint::system::System for SimpleExample {
    type State = f64;
    fn diff(&self, state: &f64, t: f64) -> f64 {
        *state
    }
}

#[test]
fn test_simple_example() {
    let mut iter = numerical::odeint::forward_euler::Iter::new(SimpleExample {}, 1.0, 0.02);
    assert_that!(iter.nth(9),
                 is(equal_to(Some((0.19999999999999998, 1.2189944199947573)))));
}

struct Vector2D {
    x: f64,
    y: f64,
}

impl Vector2D {
    fn new(x: f64, y: f64) -> Vector2D {
        Vector2D { x: x, y: y }
    }
}

impl AddAssign for Vector2D {
    fn add_assign(&mut self, a: Vector2D) {
        self.x += a.x;
        self.y += a.y;
    }
}

impl Mul<f64> for Vector2D {
    type Output = Vector2D;
    fn mul(self, rhs: f64) -> Vector2D {
        Vector2D::new(self.x * rhs, self.y * rhs)
    }
}

impl Clone for Vector2D {
    fn clone(&self) -> Vector2D {
        Vector2D::new(self.x, self.y)
    }
}

struct LessSimpleExample {}
impl numerical::odeint::system::System for LessSimpleExample {
    type State = Vector2D;
    fn diff(&self, state: &Vector2D, t: f64) -> Vector2D {
        Vector2D::new(-state.x, state.x)
    }
}

#[test]
fn test_less_simple_example() {
    let iter = numerical::odeint::forward_euler::Iter::new(LessSimpleExample {},
                                                           Vector2D::new(100.0, 0.0),
                                                           0.02);
    for (t, v) in iter.take(20) {
        println!("{:?}\t{:?}\t{:?}", t, v.x, v.y);
    }
    assert!(true);
}
