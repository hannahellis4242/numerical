#[cfg(test)]
#[macro_use]
extern crate hamcrest;
extern crate rulinalg;
use hamcrest::prelude::*;
use rulinalg::vector::Vector;

extern crate numerical;

use std::ops::{AddAssign, Mul};

struct SimpleExample {}

impl numerical::odeint::system::System for SimpleExample {
    type State = f64;
    fn diff(&self, state: &f64, t: f64) -> f64 {
        *state
    }
}

#[test]
fn test_simple_example() {
    let mut iter = numerical::odeint::runge_kutta_fourth::Iter::new(SimpleExample {}, 1.0, 0.02);
    assert_that!(iter.nth(9),
                 is(equal_to(Some((0.19999999999999998, 1.2214027578398445)))));
}

struct LessSimpleExample {
    decay_rate: f64,
}
impl numerical::odeint::system::System for LessSimpleExample {
    type State = rulinalg::vector::Vector<f64>;
    fn diff(&self, state: &rulinalg::vector::Vector<f64>, t: f64) -> rulinalg::vector::Vector<f64> {
        rulinalg::vector::Vector::new(vec![-self.decay_rate * state[0], self.decay_rate * state[0]])
    }
}

#[test]
fn test_less_simple_example() {
    let iter =
        numerical::odeint::runge_kutta_fourth::Iter::new(LessSimpleExample { decay_rate: 0.2 },
                                                         rulinalg::vector::Vector::new(vec![100.0,
                                                                                           0.0]),
                                                         0.02);
    println!("0\t100.0\t0.0\t100.0");
    for (t, v) in iter.take(200) {
        println!("{:?}\t{:?}\t{:?}\t{:?}", t, v[0], v[1], v[0] + v[1]);
    }
    assert!(true);
}
