#[cfg(test)]
#[macro_use]
extern crate hamcrest;
use hamcrest::prelude::*;

extern crate numerical;

use std::f64;

#[test]
fn test_square_root_1() {
    let value = numerical::root_find::bisection::Iter::new(|x| x * x - 2.0, (0.0, 2.0)).nth(10);
    assert_that!(value, is_not(none::<(f64, f64)>()));
    let value_unwrapped = value.unwrap();
    assert_that!(value_unwrapped.0, is(less_than(f64::sqrt(2.0))));
    assert_that!(value_unwrapped.1, is(greater_than(f64::sqrt(2.0))));
    assert_that!(value_unwrapped.1 - value_unwrapped.0,
                 is(close_to(0.0009765625, 0.0001)));
}
